from selenium import webdriver
browser = webdriver.Chrome('C:\Python3.7\chromedriver')
# browser.get('http://thecodex.me')
browser.get('https://www.seleniumhq.org/')

print("try find webpage 'class'-element downloadBox")
elem = browser.find_element_by_class_name('downloadBox')
print("found: %s" %elem)

print("Try find tag name")
print('found %s' % elem.tag_name)

print("Try find attribute name onClick")
el = elem.get_attribute('onclick')
print('found: %s' % el)


l = elem.text
print('Try find text element')
print('Found: %s' % l)


print("try find element location")
y = elem.location
print('Found : %s ' % y)

browser.find_element_by_link_text('many browsers')
u = elem.get_attribute('https')
print(u)

browser.close()