'''
Created on 24.10.2018

@author: Jarkko
'''
from tkinter import *

root = Tk()
equa = " "

equation = StringVar()
calculation = Label(root,textvariable=equation)
equation.set("Enter your equation:")
calculation.grid(columnspan = 4)

def buttonPress(num):
    global equa
    equa = equa + str(num)
    equation.set(equa)

def EqualPress():
    global equa
    total = str(eval(equa))
    equation.set(total)
    equa = ""
    
def clrear():
    global equa
    equa = " "
    equation.set("")

Button1 = Button(root, text = "1", command = lambda:buttonPress(1))
Button1.grid(row = 1, column=0)
Button2 = Button(root, text = "2", command = lambda:buttonPress(2))
Button2.grid(row = 1, column=1)
Button3 = Button(root, text = "3", command = lambda:buttonPress(3))
Button3.grid(row = 1, column=2)
Plus = Button(root, text = "+", command = lambda:buttonPress("+"))
Plus.grid(row = 1, column=3)
Button5 = Button(root, text = "4", command = lambda:buttonPress(4))
Button5.grid(row = 2, column=0)
Button6 = Button(root, text = "5", command = lambda:buttonPress(5))
Button6.grid(row = 2, column=1)
Button7 = Button(root, text = "6", command = lambda:buttonPress(6))
Button7.grid(row = 2, column=2)
Minus = Button(root, text = "-", command = lambda:buttonPress("-"))
Minus.grid(row = 2, column=3)
Button9 = Button(root, text = "7", command = lambda:buttonPress(7))
Button9.grid(row = 3, column=0)
Button10 = Button(root, text = "8", command = lambda:buttonPress(8))
Button10.grid(row = 3, column=1)
Button11 = Button(root, text = "9", command = lambda:buttonPress(9))
Button11.grid(row = 3, column=2)
Multiply = Button(root, text = "*", command = lambda:buttonPress("*"))
Multiply.grid(row = 3, column=3)

Button13 = Button(root, text = "0", command = lambda:buttonPress(0))
Button13.grid(row = 4, column=0)
Equal = Button(root, text = "=" , command = EqualPress)
Equal.grid(row = 4, column=1)
Divide = Button(root, text = "/", command = lambda:buttonPress("/"))
Divide.grid(row = 4, column=2)
Clear = Button(root, text = "C")
Clear.grid(row = 4, column=3)

root.mainloop()
