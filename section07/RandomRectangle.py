'''
Created on 24.10.2018

@author: Jarkko
'''
from tkinter import *
import random
root = Tk()

canvas = Canvas(root,width=650,height=650)
canvas.pack()

def randomRect(num):
    for i in range(0,num):
        x1 = random.randrange(300)
        y1 = random.randrange(300)
        x2 = x1 + random.randrange(300)
        y2 = y1 + random.randrange(300)
        canvas.create_rectangle(x1,y1,x2,y2)
        
randomRect(10)

root.mainloop()