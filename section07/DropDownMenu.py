'''
Created on 23.10.2018

@author: Jarkko
'''
from tkinter import *
root = Tk()

def random():
    print("This is statement")

mainMenu = Menu(root)
root.configure(menu=mainMenu)

subMenu = Menu(mainMenu)
mainMenu.add_cascade(label="File", menu=subMenu)
subMenu.add_command(label="Random Func", command=random )
subMenu.add_command(label="New File", command=random)
subMenu.add_separator()
subMenu.add_command(label="Add File", command=random)
root.mainloop()