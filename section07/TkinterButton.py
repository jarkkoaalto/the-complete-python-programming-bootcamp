'''
Created on 22.10.2018

@author: Jarkko
'''
from tkinter import *
root = Tk()

def leftClick(event):
    print('Left')
    
def rightClick(event):
    print('Right')
    
def scroll(event):
    print("Scroll")
    
def leftKey(event):
    print("Left key pressed")
    
root.geometry("500x500")  

root.bind("<Button-2>",leftClick)
root.bind("<Button-3>",rightClick)
root.bind("<Button-4>",scroll)
root.bind("<Button-5>",leftKey)

def printName(event):
    print("Hello there")  
button1 = Button(root, text="Click Me")
button1.bind("<Button-1>", printName)
button1.pack()

root.mainloop()