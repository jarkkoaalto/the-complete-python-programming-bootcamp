import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import numpy as np

fig=plt.figure()
chart = fig.add_subplot(1,1,1,projection='3d')

x = [1,2,3,4,5]
y = [3,6,2,7,5]
z = [0,0,0,0,0]

dx = np.ones(5)
dy = np.ones(5)
dz = [1,8,3,4,6]

chart.bar3d(x,y,z,dx,dy,dz)

chart.set_xlabel('X label')
chart.set_ylabel('Y label')
chart.set_zlabel('Z label')
plt.show()