import matplotlib.pyplot as plt
import numpy as np

x = np.arange(1,5)
plt.plot(x,x,x+1,x,x+2)
plt.title("This is the title of the Graph")
plt.xlabel("This is the X Axis")
plt.ylabel("This is the Y Axis")
plt.legend(["Medium","Slow","Fast"])

plt.show()