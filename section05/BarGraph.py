import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure(figsize=(7,5))
names = ["Avi","Jaboc","Michelle","Brendan"]
scores = [88,57,92,85]
scores2 = [92,68,88,90]

positions = np.arange(len(scores))

plt.bar(positions, scores, width=0.3)
plt.bar(positions+0.3, scores2, width=0.3)
plt.xlabel("Names")
plt.ylabel("Scores")
plt.title("Latest Test Scores")
plt.xticks(positions+0.15, names)

plt.show()