import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure(facecolor="g")
chart1 = fig.add_subplot(1,1,1)
#chart2 = fig.add_subplot(3,1,2)
#chart3 = fig.add_subplot(3,1,3)

x = np.arange(1,5)
chart1.plot(x,x, label="Medium", color="r")
chart1.tick_params(axis="x", color="w")
chart1.tick_params(axis="y", color="w")
for spines in chart1.spines:
    chart1.spines[spines].set_color("w")
#chart2.plot(x,x*3, label="Fast", color="g")
#chart3.plot(x,x/2, label="Slow", color="r")
#plt.xlabel("This is the X axis")
#plt.ylabel("This is the Y axis")
#plt.title("This is the title of the graph")
plt.legend(loc="best")
chart1.grid(True)
#chart2.grid(True)
#chart3.grid(True)
plt.axis([0,5,-1,13])
plt.show()