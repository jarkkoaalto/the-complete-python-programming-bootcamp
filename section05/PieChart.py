import matplotlib.pyplot as plt
import numpy as nper
plt.figure(figsize=(5,5))

labels = ["USA","India","China","Italy","Canada"]
values = [25,10,15,40,23]
explode = [0, 0, 0, .01, 0]

plt.pie(values, labels=labels, autopct="%.1f%%", explode=explode)
plt.show()