import matplotlib.pyplot as plt
import numpy as np

x = np.arange(1,5)
plt.plot(x,x,"--p",label="Medium", color="y", linewidth=5.0, marker="d")
plt.plot(x,x*3, "--p", label="Fast", color="g")
plt.plot(x, x/2, "--p", label="Slow", color="r")
plt.xlabel("This is the X Axis")
plt.ylabel("This is the Y Axis")
plt.title("This is the title of graph")
plt.legend(loc="best")
plt.grid(True)

plt.axis([0,5,-1,12])
plt.show()