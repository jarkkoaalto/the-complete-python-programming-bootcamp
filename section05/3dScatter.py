import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import numpy as np

fig = plt.figure()
chart = fig.add_subplot(1,1,1,projection='3d')

x = np.random.randn(200)
y = np.random.randn(200)
z = np.random.randn(200)
size = 200*np.random.rand(200)
chart.scatter(x,y,z,s=size)

plt.show()