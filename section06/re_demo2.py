import re

phoneRegex = re.compile(r'(\d\d\d-)?\d\d\d-\d\d\d\d')
mo = phoneRegex.search
mo = phoneRegex.search("This is my number 123-456-7890")
print(mo.group())

ma = phoneRegex.search("This is my number 456-7890")
print(ma.group())

batRegex = re.compile(r'Bat(wo)*man')
mo = batRegex.search('I am Batwoman')
print(mo.group())

mo = batRegex.search("I am Batwowowowoman")
print(mo.group())

mo = batRegex.search("I am Batman")
print(mo.group())

batRegex = re.compile(r'Bat(wo)+man')
mo = batRegex.search("I am Batwowowoman")

phoneRegex  = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')
x = phoneRegex.findall('Amy is 345-644-3345 and Bob is 456-335-2222')
print(x)

phoneRegex  = re.compile(r'(\d\d\d)-(\d\d\d)-(\d\d\d\d)')
e = phoneRegex.findall('Amy is 345-644-3345 and Bob is 456-335-2222')
print(e)

