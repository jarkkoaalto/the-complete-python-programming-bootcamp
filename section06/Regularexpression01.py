import re

matchingRegex = re.compile(r'.at')
r = matchingRegex.findall("The fat cat sat on a mat")
print(r)

# matchin everyting
nameRegex = re.compile(r'First Name: (.*)')
mo = nameRegex.search("First Name: Avinash")
print(mo.group())

allgatorRegex = re.compile(r"<.*>")
no = allgatorRegex.search("< This is > some text >")
print(no.group())

nongreedyRegex = re.compile(r'<.*?>')
so = nongreedyRegex.search(" <This is > some text >")
print(so.group())