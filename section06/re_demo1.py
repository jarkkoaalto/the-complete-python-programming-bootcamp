import re

sentence = "My phone number is 425-453-2035"
phone_number_Regex = re.compile(r'\d\d\d\-\d\d\d-\d\d\d\d')
mo = phone_number_Regex.search(sentence)
print(mo.group())

phone_number_Regex = re.compile(r'(\d\d\d)-(\d\d\d)-(\d\d\d\d)')
om = phone_number_Regex.search(sentence)
print(om.group(1))
print(om.group(2))
print(om.group(3))

sent = "I love fruits. I love all sorts of berries, and I'm a big fan of strawberries and blueberries"


fruitRegex = re.compile(r'strawberries|blueberries')
ma = fruitRegex.search(sent)
print(ma.group())
me = fruitRegex.search("blueberries and strawberries")
print(me.group())
