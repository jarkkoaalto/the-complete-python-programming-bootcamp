import schedule
import time

def helloWorld():
    print("Hello, world")

schedule.every().day.at("12:00".do(helloWorld))
schedule.every(5).minutes.do(helloWorld)
schedule.every().hour.do()
schedule.every().monday.do()


while True:
        schedule.run_pending()
        time.sleep(1)
