'''
Created on 7.10.2018

@author: Jarkko
'''
import re
from section06.re_demo2 import phoneRegex
phoneRegex = re.compile(r'\d\d\d\d-\d\d\d-\d\d\d\d')
phoneRegex = re.compile(r'\D\D\D\D')

mo = phoneRegex.search('123 ads')
print(mo.group())

regex = re.compile(r'\w\w\w')
mo = regex.search('ads')
print(mo.group())

regex = re.compile(r'\s\w+')

print(regex.findall('this is some text'))

regex = re.compile(r'\d\s\w+')
print(regex.findall("12 peaches, 15 lemons and 14 cherrie"))

vowelRegex = re.compile(r'[^AEIOUUaeeiou] ')
print(vowelRegex.findall("This is a sentence"))