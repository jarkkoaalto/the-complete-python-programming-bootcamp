import re

regex = re.compile("Banana",re.I)
mo = regex.search("banana bANANAa BananA")
print(mo.group())
print(regex.findall("banana bANANAa BananA"))


agentRegex = re.compile(r"Agent \w+")
print(agentRegex.sub("CENSORED","today at 5 PM it was reported that Agent Steve had a coffee"))
