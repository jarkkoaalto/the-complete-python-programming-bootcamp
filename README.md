# The-Complete-Python-Programming-Bootcamp

About this course
Learn everything about Python from the Basics to File and Browser Automation, Python GUI, Data Analysis and more!
Course content


### Section 2: Introduction to Python Programming
### Section 3: File Input / Output and Automation with Word and Excel
### Section 4: Web Scraping, Parsing and Browser Automation
### Section 5: Data Analysis and Visualization - Graphs and Charts
### Section 6: Regular Expressions and Task Management
### Section 7: Python GUI and Gaming
