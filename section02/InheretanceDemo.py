class Parent:
    def __init__(self):
        print("This is theh parent class")
    def parentFunc(self):
        print("This is the parent func")


p = Parent()
print(p)
print(p.parentFunc())

class Child(Parent):
    def __init__(self):
        pass
        print("This is the child Class")
    def childFunction(self):
        print("This is the child func")
    def test(self):
        print("child")

c = Child()
print(c)


class Par:
    def __init__(self):
        pass
    def test(self):
        print("parent")

p = Par()
c1 = Child()
print(c1.test())

print(p.test())