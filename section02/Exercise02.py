# The code exercises
# 1. Create a list of names and print the second item
print()
list1 = ["Orange", "banana","tulostettava", "mango", "kiwi"]

print(list1[2])
print()
# 2. Create list of sport and then replave the second item with another sport
list2 =["Hockey","foodball","baseball", "Rugby"]
list2[1] = "Dart"
print(list2)
print()
# 3. Create a list containing numbers and delete the fifth nnumber from the array
list3 = [3,4,5,6,7,8,9,2,1]
print(list3)
del list3[5]
print(list3)
print()
# 4. Create two list of numbers and merge them
list4 = [1,2,3,4,5]
list5 = [6,7,8,9,0]
print(list4+list5)
print()
# 5. Create an list of numbers and find the length, minimum and maximum
list6 = [2,3,4,5,6,7,12,23,34,45]
print(len(list6))
print(min(list6))
print(max(list6))
print()
# 6. Create dictionart of students and score print out students score
students = {"mark":24,"ben":34,"mart":34, "mary":44}
print(students["mark"])
print(students["ben"])
print(students["mart"])
print(students["mary"])
print()
# 7. Create dictionary with the key being names and values being ages and then delete the second key/value pair
students1 = {"mark":24,"ben":34,"mart":34, "mary":44}
print(students1)

# Exercise #8
data = {"Rachel": 17, "Rahul": 61, "Anna": 15}
print(data.keys)
print(data.values)

# Exercise #9
movies = ("Inception", "The Imitation Game", "The Secret Life of Walter Mitty")
print(movies)

# Exercise #10
data = (1, 2, 3)
print(data[0:2])
