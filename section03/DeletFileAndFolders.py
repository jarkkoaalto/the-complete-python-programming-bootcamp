import os
import shutil

#os.unlink('C:\\Users\\Jarkko\\Python-harjoitukset\\PythonBootCamp\\section03\\pythontext.txt') // empty file
#os.rmdir('C:\\Users\\Jarkko\\Python-harjoitukset\\PythonBootCamp\\section03\\removeme') \\ empty directories


## Remove directory and all files inside directories
shutil.rmtree('C:\\Users\\Jarkko\\Python-harjoitukset\\PythonBootCamp\\section03\\Test', ignore_errors=True)