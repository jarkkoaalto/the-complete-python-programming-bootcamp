import os

# print(os.getcwd())

## Change directory
# os.chdir('C:\\Users\\Jarkko\\Python-harjoitukset')
# os.getcwd()

# print(os.listdir('.'))

myFile = open('test.txt')
print(myFile.read())

myFile.close()

myFile = open('test.txt')
print(myFile.readlines())
myFile.close()

myFile = open('test.txt','w')
myFile.write('Hello world \n')
myFile.close()

myFile = open('test.txt','a')
myFile.write("This is some new content\n")
myFile.close()

print(myFile)