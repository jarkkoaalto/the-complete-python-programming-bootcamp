import openpyxl

wb = openpyxl.Workbook()
sheet = wb.active

sheet.row_dimensions[1].height = 100
sheet.column_dimensions['B'].width = 60

sheet['A1'] = "Tall Row"
sheet['B2'] = "Wide Column"
wb.save('new.xlsx')