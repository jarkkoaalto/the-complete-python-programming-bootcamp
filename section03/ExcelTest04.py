import openpyxl

wb = openpyxl.load_workbook('Cheese.xlsx')
sheet = wb.get_sheet_by_name('Cheese')

sheet.merge_cells('A1:C1')

sheet.freeze_panes = 'A2'
wb.save('Cheese.xlsx')
