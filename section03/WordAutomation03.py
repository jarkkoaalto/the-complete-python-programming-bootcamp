import docx

doc = docx.Document("NewDoc.docx")

doc.add_heading('Header 0',0)
doc.add_heading('Header 1',1)
doc.add_heading('Header 2',2)
doc.add_heading('Header 3',3)
doc.add_heading('Header 4',4)
doc.save("NewDoc.docx")

doc = docx.Document("NewDoc.docx")
doc.add_picture('emm.jpg', width=docx.shared.Inches(2), height=docx.shared.Inches(3))
doc.save("NewDoc.docx")